<%@page import="com.xnx3.j2ee.util.SystemUtil"%>
<%@page import="com.xnx3.wangmarket.admin.G"%>
<%@page import="com.xnx3.j2ee.Global"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/wm/common/head.jsp">
	<jsp:param name="title" value="参数设置"/>
</jsp:include>

<style>
	
	.content{
		width: 600px;
		min-height:80%;
   margin: 0 auto;
   box-shadow: rgba(0, 0, 0, 0.06) 0px 1px 10px 2px;
   padding: 30px;
   margin-top: 50px;
	}
	.title{
		border-bottom: 1px solid #eee;
   padding-top: 20px;
   padding-left: 10px;
   padding-bottom: 20px;
   font-size: 28px;
   margin-bottom: 20px;
   text-align:center;
	}
	.content ul{
		padding-left: 20px;
	}
	.content ul li{
		list-style-type: decimal;
		padding-left:10px;
		padding-bottom:4px;
	}
	.content ul li img{
		max-width:250px;
		padding:4px;
		padding-left:40px;
	}
	.info{
		font-size:14px;
		line-height: 22px;
	}
	.info h2,h3,h4,h5{
	border-bottom: 1px solid #eee;
   padding-top: 23px;
   margin-bottom: 10px;
   padding-bottom: 5px;
	}
	
	@media only screen and (max-width: 700px) {
		.content{
			width:auto;
			margin-top: 0px;
			box-shadow: rgba(0, 0, 0, 0.06) 0px 0px 0px 0px;
		}
		
	}
	
	a{
		color: blue;
	}
</style>

<div class="content">
	<div class="title">
		华为云OBS对象存储-自动创建 Bucket Name
	</div>
	
	<div class="info" style="font-size:16px;">
		<div style="float:left; width:70%;">bucket name:<%=SystemUtil.get("HUAWEIYUN_OBS_BUCKETNAME") %></div>
		<div style="float:left" id="obs"></div>
	</div>
</div>

<script>
	//华为云日志服务预留方法
	<%-- var slsObj = document.getElementById('sls');
	if('<%=Global.get("HUAWEIYUN_SLS_USE")  %>' == '1'){
		slsObj.innerHTML = "已使用";
	}else{
		//未使用
		slsObj.innerHTML = '未使用&nbsp;&nbsp; <button onclick="createSLS();">创建此组建云服务并启用</button>';
	} --%>
	
	//OBS检测
	var obsObj = document.getElementById('obs');
	if('<%=SystemUtil.get("ATTACHMENT_FILE_MODE") %>' == 'huaWeiYunOBS'){
		obsObj.innerHTML = "已创建";
	}else{
		//未使用
		obsObj.innerHTML = '<button onclick="createOBS();" class="layui-btn layui-btn-primary">请点此自动创建</button>';
	}

	//创建OBS服务及自动配置
	function createOBS(){
		msg.loading("创建中");	//显示“更改中”的等待提示
		post("/plugin/huaWeiYunServiceCreate/obs/create.json", {}, function(data){
			msg.close();	//关闭“更改中”的等待提示
			if(data.result != '1'){
				msg.failure(data.info);
			}else{
				msg.success("创建成功",function(){
					if(<%=SystemUtil.get("IW_AUTO_INSTALL_USE").equalsIgnoreCase("true") %>){
						//是在刚开始安装时使用，那么跳转到下一步，配置域名
						window.location.href='/plugin/huaWeiYunServiceCreate/set/setCdn.jsp';
					}else{
						//是在安装后使用
						msg.info('您已成功创建<br/>当前插件任务已完成。');
					}
				});
			}
		});
	}
	
	
	//华为云日志服务预留方法
	/* function createSLS(){
		parent.iw.loading("创建中");	//显示“更改中”的等待提示
		$.post(
			"/plugin/huaWeiYunServiceCreate/sls/create.do",  function(data){
				parent.iw.loadClose();	//关闭“更改中”的等待提示
				if(data.result != '1'){
					parent.iw.msgFailure(data.info);
				}else{
					parent.iw.msgSuccess("已创建SLS日志服务！1分钟后生效");
					window.onload();	//刷新页面
				}
			}, 
		"json");
	} */

	
var bucketname = '<%=SystemUtil.get("HUAWEIYUN_OBS_BUCKETNAME") %>';
if(bucketname.length > 6){
	msg.info('检测到您OBS已配置完成，即将进入下一步...',function(){
		window.location.href='/plugin/huaWeiYunServiceCreate/set/setCdn.jsp';
	});
}
</script>


<jsp:include page="/wm/common/foot.jsp"></jsp:include>  