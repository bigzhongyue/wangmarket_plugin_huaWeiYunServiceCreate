<%@page import="com.xnx3.j2ee.util.SystemUtil"%>
<%@page import="com.xnx3.wangmarket.admin.G"%>
<%@page import="com.xnx3.j2ee.Global"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="/wm/common/head.jsp">
	<jsp:param name="title" value="参数设置"/>
</jsp:include>

<style>
	
	.content{
		width: 600px;
		min-height:80%;
   margin: 0 auto;
   box-shadow: rgba(0, 0, 0, 0.06) 0px 1px 10px 2px;
   padding: 30px;
   margin-top: 50px;
	}
	.title{
		border-bottom: 1px solid #eee;
   padding-top: 20px;
   padding-left: 10px;
   padding-bottom: 20px;
   font-size: 28px;
   margin-bottom: 20px;
   text-align:center;
	}
	.content ul{
		padding-left: 20px;
	}
	.content ul li{
		list-style-type: decimal;
		padding-left:10px;
		padding-bottom:4px;
	}
	.content ul li img{
		max-width:250px;
		padding:4px;
		padding-left:40px;
	}
	.info{
		font-size:14px;
		line-height: 22px;
	}
	.info h2,h3,h4,h5{
	border-bottom: 1px solid #eee;
   padding-top: 23px;
   margin-bottom: 10px;
   padding-bottom: 5px;
	}
	
	@media only screen and (max-width: 700px) {
		.content{
			width:auto;
			margin-top: 0px;
			box-shadow: rgba(0, 0, 0, 0.06) 0px 0px 0px 0px;
		}
		
	}
	
	a{
		color: blue;
	}
</style>

<div class="content">
	<div class="title">
		设置华为云OBS对象存储服务所在区域
	</div>
	
	<div class="info" style="font-size:16px;">
	
		<div style="float:left; width:50%;">endpoint:<%=SystemUtil.get("HUAWEIYUN_COMMON_ENDPOINT") %></div>
		<div style="float:left">
			点此设置：
			<select id="selectId" onchange="selectChange();" class="layui-btn layui-btn-primary">
				<option value="" >请选择服务区域</option>
				<option value="cn-north-4" >华北-北京四</option>
				<option value="cn-north-1" >华北-北京一</option>
				<option value="cn-east-2" >华东-上海二</option>
				<option value="cn-east-3" >华东-上海三</option>
				<option value="cn-south-1" >华南-广州</option>
				<option value="cn-southwest-2" >西南-贵阳一</option>
				<option value="ap-southeast-1" >亚太-香港</option>
				<option value="ap-southeast-3" >亚太-新加坡</option>
				<option value="ap-southeast-2" >亚太-曼谷</option>
			</select>
		</div>
	</div>
</div>



<script type="text/javascript">
//当选择区域之后，触发
function selectChange(){
	var objS = document.getElementById("selectId");
	if(objS.value.length < 2){
		return;
	}
	
	msg.loading("更改中");	//显示“更改中”的等待提示
	post("/plugin/huaWeiYunServiceCreate/set/setAreaSave.json", { "area": objS.value}, function(data){
		msg.close();	//关闭“更改中”的等待提示
		if(data.result != '1'){
			msg.failure(data.info);
		}else{
			msg.success("操作成功", function(){
				window.location.href="../index.do";
			});
		}
	});
}
</script>

<jsp:include page="/wm/common/foot.jsp"></jsp:include> 