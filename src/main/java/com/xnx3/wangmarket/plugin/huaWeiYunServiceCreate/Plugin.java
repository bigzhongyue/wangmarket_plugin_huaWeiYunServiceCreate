package com.xnx3.wangmarket.plugin.huaWeiYunServiceCreate;

import com.xnx3.j2ee.pluginManage.PluginRegister;

/**
 * 华为云自动创建一些服务,尚未开发完成,未在功能插件栏中显示.
 * @author 李鑫
 */
@PluginRegister(menuTitle = "华为云OBS", intro = "自动开通华为云OBS服务", menuHref="../../plugin/huaWeiYunServiceCreate/index.do", applyToSuperAdmin=true, version="1.6", versionMin="5.5")
public class Plugin{
}