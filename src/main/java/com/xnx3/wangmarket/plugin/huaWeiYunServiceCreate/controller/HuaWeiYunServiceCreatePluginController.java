package com.xnx3.wangmarket.plugin.huaWeiYunServiceCreate.controller;
import javax.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xnx3.DateUtil;
import com.xnx3.j2ee.entity.System;
import com.xnx3.j2ee.pluginManage.controller.BasePluginController;
import com.xnx3.j2ee.service.SystemService;
import com.xnx3.j2ee.util.SystemUtil;
import com.xnx3.j2ee.vo.BaseVO;

/**
 * 华为云环境配置入口
 * @author 李鑫
 */
@Controller
@RequestMapping("/plugin/huaWeiYunServiceCreate/")
public class HuaWeiYunServiceCreatePluginController extends BasePluginController {
	
	@Resource
	SystemService systemService;
	
	/**
	 * 总管理后台-功能插件进来的首页
	 */
	@RequestMapping("index.do")
	public String index(Model model){
		// 刷新本地缓存
		systemService.refreshSystemCache();
		
		//是否是第一次使用还未安装，正在安装界面
		if(SystemUtil.get("IW_AUTO_INSTALL_USE").equalsIgnoreCase("true")) {
			//正在安装界面安装
		}else if(!haveSuperAdminAuth()) {
			// 是否有管理员权限
			return error(model, "无权使用");
		}
		
		// 是否设置了地域信息，没有的话再检查又没有设置华为秘钥信息
		if(SystemUtil.get("HUAWEIYUN_COMMON_ENDPOINT") != null && SystemUtil.get("HUAWEIYUN_COMMON_ENDPOINT").length() > 6){
			return redirect("plugin/huaWeiYunServiceCreate/service.jsp");
		}else{
			return redirect("plugin/huaWeiYunServiceCreate/set/setAccessKey.do");
		}
	}
	
	/**
	 * 此接口的作用仅仅只是为了判断是否有这个插件而已。
	 */
	@RequestMapping("isExist.json")
	@ResponseBody
	public BaseVO isExist(){
		return success();
	}
	
}